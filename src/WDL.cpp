#include "WDL.hpp"

namespace WDL {
    WDL::WDL(Uint32 flags) {
        int success = SDL_Init(flags);
        if (success < 0) {
            throw SDL_GetError(); 
        }
    }

    WDL::~WDL() {
	SDL_Quit();
    }
}
