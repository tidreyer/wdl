#include <string>
#include <SDL2/SDL.h>

namespace WDL {
    class Window;
    class Surface;

    class Window {
        public:
            Window(std::string, int, int, int, int, Uint32);
            ~Window();
            Surface GetSurface();
            void UpdateSurface();
        private:
            SDL_Window* mWindow;
    };

    class Surface {
        public:
            Surface(SDL_Surface*);
            ~Surface() = default;
            void FillRect(const SDL_Rect*, Uint32 );
            SDL_PixelFormat* format();
        private:
            SDL_Surface* mSurface;
    };
}
