#include <exception>

#include "WDL_video.hpp"

namespace WDL {
    Window::Window(std::string title,
            int x, int y, int w, int h,
            Uint32 flags)
    {
        mWindow = SDL_CreateWindow(title.c_str(), x, y, w, h, flags);
        if (mWindow == nullptr)
            throw SDL_GetError(); 
    }

    Window::~Window() {
        SDL_DestroyWindow(mWindow);
    }

    Surface Window::GetSurface() {
        SDL_Surface* ptr = SDL_GetWindowSurface( mWindow );
        return Surface(ptr);
    }

    void Window::UpdateSurface() {
        SDL_UpdateWindowSurface( mWindow );
    }

    Surface::Surface(SDL_Surface* ptr)
    {
        mSurface = ptr;
    }

    SDL_PixelFormat* Surface::format() {
        return mSurface->format;
    }

    void Surface::FillRect( const SDL_Rect* rect, Uint32 color ) {
        SDL_FillRect( mSurface, rect, color );
    }
}
