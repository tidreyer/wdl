//Using SDL and standard IO
#include <SDL2/SDL.h>
#include "../src/WDL.hpp"
#include "../src/WDL_video.hpp"

//Screen dimension constants
constexpr int SCREEN_WIDTH = 640;
constexpr int SCREEN_HEIGHT = 480;

int main( int argc, char* args[] )
{
    // Initializing WDL (takes care of SDL_Init() and SDL_Quit())
    WDL::WDL wdl_instance {SDL_INIT_VIDEO};

    // The window we'll be rendering to
    WDL::Window window { "WDL Basic Example",
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        SCREEN_WIDTH, SCREEN_HEIGHT,
        SDL_WINDOW_SHOWN };

    // The surface contained by the window
    WDL::Surface screenSurface = window.GetSurface();

    //Fill the surface white
    screenSurface.FillRect(nullptr, SDL_MapRGB( screenSurface.format(), 0xFF, 0xFF, 0xFF ));
    
    // Update the window
    window.UpdateSurface();

    // Wait two seconds
    SDL_Delay( 2000 );

    return 0;
}
