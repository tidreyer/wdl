# SDL Wrapper

A wrapper layer around SDL to make the functionality available in a modern C++
programming style.

All functionality should be available in a similar syntax as with direct SDL,
but under the namespace `WDL` (Wrapped DirectMedia Layer) instead of `SDL`.
Source code files follow the names given in SDL, replacing the `SDL_` prefix
with `WDL_`.
